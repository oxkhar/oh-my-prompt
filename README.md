# Tool to setup prompt bash with git status (and more...)

This prompt is a idea from ["Informative git prompt for bash"](https://github.com/magicmonty/bash-git-prompt)

A **bash** prompt that displays information about the current git repository.
In particular the branch name, remote name, difference with remote branch, number of files staged, changed, etc.

Goal is that all process is one bash file where git information is provided with shell sentences. It's fast and only one file to include and configuration.

##  Prompt Structure

By default, the general appearance of the prompt is:

    [<branch> <upstream> <branch tracking>|<action in curse> <local status>]

The symbols are as follows:

- Branch symbols:
    - When the branch name starts with a colon ``:``, it means it's actually a hash, not a branch (although it should be pretty clear, unless you name your branches like hashes :) )
    - ``⚑``: there are ``n`` stahes. If is underline means that some stashes was made on current branch
- Local status symbols
    - ``✔``: repository clean
    - ``●n``: there are ``n`` staged files
    - ``✖n``: there are ``n`` unmerged files
    - ``✚n``: there are ``n`` changed but *unstaged* files
    - ``…n``: there are ``n`` untracked files
- Branch tracking symbols
    - ``.``: branch is locale, else remote name is showed
    - ``↑·n``: ahead of remote by ``n`` commits
    - ``↓·n``: behind remote by ``n`` commits
    - ``↓·m↑·n``: branches diverged, other by ``m`` commits, yours by ``n`` commits

## Examples

The prompt may look like the following:

 * ``(master origin↑3|✚1)``: on branch ``master``, tracks ``origin``/master, ahead of remote by 3 commits, 1 file changed but not staged
 * ``(status .|●2)``: on branch ``status``, 2 files staged
 * ``(master origin|✚7…2)``: on branch ``master``, 7 files changed, some files untracked
 * ``(master origin|✖2✚3)``: on branch ``master``, 2 conflicts, 3 files changed
 * ``(experimental origin↓·2↑·3|✔)``: on branch ``experimental``; your branch has diverged by 3 commits, remote by 2 commits; the repository is otherwise clean
 * ``(:70c2952|✔)``: not on any branch; parent commit has hash ``70c2952``; the repository is otherwise clean
 * ``(master origin|~MERGE~●2)``: Merge is currently in action and it needs to be resolve

## Install

 * Clone this repository to your homedir, e.g.
```bash
git clone https://gitlab.com/oxkhar/oh-my-prompt.git ~/.oh-my-prompt
```

 * Source the file ```prompt.sh``` from your ```~/.bashrc``` (or ```~/.bash_aliases```) file
 ```bash
 . ~/.oh-my-prompt/prompt.sh
```

 * Go in a git repository and ``test it!``

## Config

The best way to setup your prompt is creating ``OMP_PROMPT`` variable with a config literal.
To view git status you only have to put ``\g`` escape code into prompt string.

```bash
OMP_PROMPT="\u@\h:\w \g \$ "
```

This are the scape codes you can set...

  * ``\g`` : Git status
  * ``\p`` : Python virtual enviroment
  * ``\c`` : Return code of last command execution

More complete example...

```bash
OMP_PROMPT=\
"╭─${debian_chroot:+($debian_chroot)}"\
"\[\e[01;32m\]\u@\h\[\e[00m\] "\
"\[\e[01;34m\]\w\[\e[00m\] "\
"\g \c \n"\
"╰─\$ "
```
