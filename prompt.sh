#!/bin/bash

function omp_git_prompt() {
    # if not is a git project exit
    omp_git_repo_is_available || return 1
    # Config prompt symbols
    local  \
        prompt_prefix="["       \
        prompt_head=""          \
        prompt_stashes="⚑"      \
        prompt_remote=" "       \
        prompt_ahead="↑·"       \
        prompt_behind="↓·"      \
        prompt_separator="|"    \
        prompt_action="~"       \
        prompt_staged="●"       \
        prompt_conflicts="✖"    \
        prompt_changed="✚"      \
        prompt_untracked="…"    \
        prompt_clean="✔"        \
        prompt_suffix="]"
    # Config colors
    local  \
        color_prefix=""                 \
        color_head="fg_purple bold"     \
        color_stashes="fg_blue bold"    \
        color_remote="fg_white bold"    \
        color_ahead=""                  \
        color_behind=""                 \
        color_separator=""              \
        color_action="fg_cyan bold"     \
        color_staged="fg_yellow"        \
        color_conflicts="fg_red"        \
        color_changed="fg_blue"         \
        color_untracked="fg_white"      \
        color_clean="fg_green bold"     \
        color_suffix=""
    # Status values
    local action head commit branch remote ref_merge ref_remote
    local -i ahead behind stashes stashes_on_branch  \
             changed clean conflicts staged untracked

     # Generate status data
    read head commit branch <<< $(omp_git_current_head)
    read stashes stashes_on_branch <<< $(omp_git_stashes "$branch")
    read remote ref_remote ref_merge <<< $(omp_git_remote_refs "$branch")
    read ahead behind <<< $(omp_git_remote_diffs "$ref_remote" "$ref_merge")
    read action <<< $(omp_git_current_action)
    read clean changed staged untracked conflicts <<< $(omp_git_status_files)

    # Build prompt
    format prefix "{prompt}"
    format head

    (( $stashes_on_branch > 0 )) && color_stashes+=" underline"
    format_if not_zero stashes

    format_if not_empty remote
    format_if not_zero behind
    format_if not_zero ahead

    format separator "{prompt}"

    format_if not_empty action "{prompt}{value}{prompt}"

    if (( $clean == 0 )); then
        format_if not_zero staged
        format_if not_zero conflicts
        format_if not_zero changed
        format_if not_zero untracked
    else
        format clean "{prompt}"
    fi

    format suffix "{prompt}"
}

function omp_git_repo_is_available() {
    git rev-parse --git-dir 2> /dev/null 1>&2
}

# @return head Reference of actual position of head
# @return commit Current sort SHA1
# @return branch Branch name where we stay
function omp_git_current_head() {
    local commit=$(git rev-parse --short HEAD 2> /dev/null)

    local branch=$(git symbolic-ref -q HEAD 2> /dev/null)
    branch=${branch/refs\/heads\//}

    local head=$branch
    if [[ -z $head ]]; then
        head=":"$commit
    fi

    debug && (
        colorize fg_green "Head:"; colorize " $head "
        echo
    ) >&2

    echo -n "$head" "$commit" "$branch"
}

# @param $1 Branch to search stashes
# @return stashes Number stashes saved
# @return stashes_on_branch Number stashes over a given branch
function omp_git_stashes() {
    local branch=${1?'param "branch" is null'}

    local -i stashes_on_branch=0
    local -i stashes=$(git stash list 2> /dev/null | wc -l)

    if [[ -n $branch && $stashes -gt 0 ]]; then
        stashes_on_branch=$(git stash list 2> /dev/null | grep " [Oo]n ${branch}:" | wc -l)
    fi

    debug && (
        colorize fg_green "Stashes:"; colorize " $stashes_on_branch/$stashes "
        echo
    ) >&2

    echo -n $stashes $stashes_on_branch
}

# @param $1 Reference to remote
# @param $2 Reference to merge
# @return ahead Number of local commits ahead from remote repository
# @return behind Number of local commits behind from remote repository
function omp_git_remote_diffs() {
    local ref_remote=${1?'param "ref_remote" is null'}
    local ref_merge=${2?'param "ref_merge" is null'}
    local -i ahead behind
    local list_rev

    list_rev=$(git rev-list --left-right ${ref_remote}...HEAD 2> /dev/null)
    if (( $? == 128 )); then
        list_rev=$(git rev-list --left-right ${ref_merge}...HEAD 2> /dev/null)
    fi

    ahead=$(  grep "^>"    <<< "$list_rev" | wc -l )
    behind=$( grep -Ev "(^>|^$)" <<< "$list_rev" | wc -l )

    debug && (
        colorize fg_red "Ahead:"; colorize " $ahead "
        colorize fg_red "Behind:"; colorize " $behind "
        echo
    ) >&2

    echo -n $ahead $behind
}

# @param $1 Branch to search remote info
# @return remote Name remote repository associated at branch
# @return ref_merge Reference of branch local to merge from repository
# @return ref_remote Reference of remote branch associted
function omp_git_remote_refs() {
    local branch=${1?'param "branch" is null'}
    local remote ref_remote ref_merge

    [[ -z $branch ]] && return 0

    remote=$(git config  branch.${branch}.remote)
    if [[ -n $remote ]]; then
        ref_merge=$(git config branch.${branch}.merge)
    else
        remote="."
        ref_merge=$(git rev-parse --symbolic-full-name ${branch} 2> /dev/null)
    fi

    if [[ $remote == "." ]]; then
        ref_remote=$ref_merge
    else
        ref_remote=$(git rev-parse --symbolic-full-name ${remote}/${branch} 2> /dev/null)
    fi

    debug && (
        colorize fg_yellow "Remote:"; colorize " $remote "
        colorize fg_yellow "Merge:"; colorize " $ref_merge "
        colorize fg_yellow "Ref:"; colorize " $ref_remote "
        echo
    ) >&2

    echo -n "$remote" "$ref_remote" "$ref_merge"
}

# @return action Current command in action (merging, rebase, ...)
function omp_git_current_action () {
    local action=""

    # Inside git dir don't show any status
    if [[ $(git rev-parse --is-inside-git-dir) == "true" ]]; then
        echo -n "GIT_DIRECTORY"
        return 0
    fi

    local git_dir="$(git rev-parse --git-dir)"

    if [ -f "$git_dir/rebase-merge/interactive" ]; then
        action="REBASE_INTERACTIVE"
    elif [ -d "$git_dir/rebase-merge" ]; then
        action="REBASE_MERGE"
    elif [ -d "$git_dir/rebase-apply" ]; then
        if [ -f "$git_dir/rebase-apply/rebasing" ]; then
            action="REBASE"
        elif [ -f "$git_dir/rebase-apply/applying" ]; then
            action="MAILBOX"
        else
            action="MAILBOX/REBASE"
        fi
    elif [ -f "$git_dir/MERGE_HEAD" ]; then
        action="MERGE"
    elif [ -f "$git_dir/CHERRY_PICK_HEAD" ]; then
        action="CHERRY-PICK"
    elif [ -f "$git_dir/BISECT_LOG" ]; then
        action="BISECT"
    fi

    debug && (
        colorize fg_cyan "Action:"; colorize " $action "
        echo
    ) >&2

    echo -n "$action"
}

# @return clean Value one when there is no changes in any files
# @return changed Number of files modified in work area
# @return staged Number os files added in stage area
# @return untracked Number of new files without add to repository
# @return conflicts Number of files with some conflicts when were merged
function omp_git_status_files() {
    [[ $(git rev-parse --is-inside-git-dir) == "true" ]] && return 0

    local -i clean changed staged untracked conflicts

    local status=$(git status --porcelain 2> /dev/null) || return 0

    staged=$(    grep -E "(^[MARC][ MD]|^[D][ M])" <<< "$status" | wc -l )
    changed=$(   grep    "^[ MARC][MD]"            <<< "$status" | wc -l )
    untracked=$( grep    "^??"                     <<< "$status" | wc -l )
    conflicts=$( grep -E "(^U|^.U|^DD|^AA)"        <<< "$status" | wc -l )

    if (( $staged == 0 && $conflicts == 0 && $changed == 0 && $untracked == 0 )); then
        clean=1
    else
        clean=0
    fi

    debug && (
        colorize fg_purple "Staged:"; colorize " $staged "
        colorize fg_purple "Conflicts:"; colorize " $conflicts "
        colorize fg_purple "Modified:"; colorize " $changed "
        colorize fg_purple "Untracked:"; colorize " $untracked "
        colorize fg_purple "Clean:"; colorize " $clean "
        echo
    ) >&2

    echo -n $clean $changed $staged $untracked $conflicts
}

### Console formater & colors
function format_if() {
    local condition=${1?}
    local name_value=${2?}
    local format=${3:-}

    [[ $condition == "not_empty" && ${!name_value} == "" ]] && return 1
    [[ $condition == "not_zero"  && ${!name_value} -eq 0 ]] && return 1

    format $name_value "$format"
}

function format() {
    local name_value=${1?}
    local format=${2:-"{prompt}{value}"}

    local name_color="color_${name_value}"
    local name_prompt="prompt_${name_value}"

    format=${format//"{prompt}"/${!name_prompt:-}}
    format=${format//"{value}"/${!name_value:-}}

    colorize -p ${!name_color} "${format}"
}

function colorize() {
    # special
    local -i  \
        reset=0         \
        bold=1          \
        underline=4
    #foreground
    local -i  \
        fg_black=30     \
        fg_red=31       \
        fg_green=32     \
        fg_yellow=33    \
        fg_blue=34      \
        fg_purple=35    \
        fg_cyan=36      \
        fg_white=37
    #background
    local -i  \
        bg_black=40     \
        bg_red=41       \
        bg_green=42     \
        bg_yellow=43    \
        bg_blue=44      \
        bg_purple=45    \
        bg_cyan=46      \
        bg_white=47

    local for_prompt=""
    [[ "${1}" == "-p" ]] && for_prompt="true" && shift

    local -i num_params=${#@}
    local text=${1}
    local color=$reset
    local code=''

    while [[ -n "${2:-}" ]]; do
        code=${!1:-0}
        if [[ -n "${code}" ]]; then
            (( ${code} == 1 || ${code} == 0 )) && \
                color=${color/?/$code}     || \
                color+=";"$code
        fi
        text=${2}
        shift
    done

    if (( ${num_params} > 1 )); then
        color=( "\e[${color}m" "\e[${reset}m" )
        [[ -n $for_prompt ]] && color=( "\[${color[0]}\]" "\[${color[1]}\]" )
        text="${color[0]}${text}${color[1]}"
    fi

    echo -en "${text}"
}

function debug() {
    [[ "${OMP_PROMPT_DEBUG:-}" == "true" ]]
}

### Prompts
function omp_update_return_code() {
    local return_code=${1?'param "return_code" is null'}

    local color_return_code="bold fg_red"
    local prompt_return_code="↵"

    format_if not_zero "return_code" " \t{value}{prompt}"
}

function omp_update_prompt_git() {
    local esc_git='\g'

    PS1="${PS1//"$esc_git"/$(omp_git_prompt)}"
}

# Determine active Python virtualenv details.
function omp_python_prompt () {
    [[ -z "${VIRTUAL_ENV:-}" ]] && return 0

    local color_virtualenv=fg_blue
    local virtualenv=$(basename "${VIRTUAL_ENV}")

    format_if not_empty virtualenv
}

function omp_update_prompt_python () {
    local esc_python='\p'

    PS1="${PS1//"$esc_python"/$(omp_python_prompt)}"
}

function omp_k8s_prompt() {
    local color_context="fg_green bold"
    local prompt_context="✱"

    local context=$(kubectl config current-context)

    colorize "["
    format_if not_empty context "{prompt}{value} 🐳"
    colorize "]"
}

function omp_update_prompt_k8s() {
    local esc_k8s='\k'

    PS1="${PS1//"$esc_k8s"/$(omp_k8s_prompt)}"
}

function omp_update_prompt() {
    local -i return=$?
    local esc_return_code='\c'
    PS1="${OMP_PROMPT//"$esc_return_code"/$(omp_update_return_code $return)}"

    ### For new prompts... \b \f \i \m \o \q \x \y \z
    omp_update_prompt_git
    omp_update_prompt_python
    omp_update_prompt_k8s

    for p in "${OMP_UPDATE_PROMPT[@]:-}"; do $p; done
}

OMP_UPDATE_PROMPT=()
: ${OMP_PROMPT="$PS1"}

PROMPT_COMMAND="omp_update_prompt"
